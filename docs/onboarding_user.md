Any Gitlab.com user is welcome and encouraged to join DrupalSpoons. Doing so lets you add/edit code, and add/edit issues.

#### Requester
1. Prerequisite: You must be logged into Gitlab.com.
1. [Create an issue in this Webmasters project](https://gitlab.com/drupalspoons/webmasters/-/issues/new?issue%5Btitle%5D=Join+DrupalSpoons). Issue title should be _Join DrupalSpoons_. No other fields are needed. Ignore _Similar Issues_ suggestions.
1. You are done!

#### DrupalSpoons Admin
Joins are now automatically processed by a [webhook listener script](https://gitlab.com/drupalspoons/webmasters/-/blob/master/webhooks/webhook_new_user.php). In case manual processing is needed, the steps are:

1. Browse to [Run Pipeline](https://gitlab.com/drupalspoons/webmasters/pipelines/new?var[PROJECT_ID]=18087244&var[ISSUE_ID]).
    - Use `master` branch.
    - Enter the project integer as shown on project homepage. Usually its the webmasters ID.
    - Enter the issue number as `ISSUE_ID`.
    - Click _Run Pipeline_. The Pipeline does:
        1. Grant *Reporter* role in [DrupalSpoons](https://gitlab.com/drupalspoons/-/group_members) group. Allows requester to edit any issue's metadata. Requester cannot push code.
        1. Grant *Developer* role in [DrupalForks](https://gitlab.com/drupalforks/-/group_members) group. Requester has permission and blessing to push to any branch in any project in DrupalForks.
1. _Close_ the issue with the following comment:
```

     Welcome to DrupalSpoons. You may now add/edit issues, or commit to any project in the [DrupalForks](https://gitlab.com/drupalforks). A few tips:

   - Clone projects from their _DrupalForks_ repo. For example, see [Devel's Clone button](https://gitlab.com/drupalforks/devel) - its blue.
   - Merge requests are used to move code from DrupalForks to its upstream in DrupalSpoons.
   - If you want to preserve your git checkouts for projects that have moved to DrupalSpoons, run `git remote set-url origin https://gitlab.com/drupalforks/[SLUG].git`. Replace _SLUG_ with the project machine name. You will now push and pull from DrupalForks instead of git.drupalcode.org.
   - [Migrate a contrib project to DrupalSpoons](https://gitlab.com/drupalspoons/webmasters/-/blob/master/docs/onboarding_project.md)

/label ~migration

```
