[[_TOC_]]

#### Goals
- The DrupalSpoons collaboration platform powers hundreds of Drupal projects and delights thousands of Drupal users.
- DrupalSpoons proves the concept (POC) that _mostly vanilla_ Gitlab is viable for the next phase of [git.drupalcode.org](https://git.drupalcode.org/). Read more about [DrupalSpoons relationship to drupalcode.org](#drupalspoons-relationship-to-drupalcodeorg).

Next steps: [Join DrupalSpoons as a Developer](docs/onboarding_user.md), [Migrate your project](docs/onboarding_project.md), or help on [Webmasters issues](https://gitlab.com/drupalspoons/webmasters/-/issues).

#### Collaboration Model
A great strengths of drupal.org issues is that they provide a single URL for each change. Patches are there, reviews are there, etc. The Github/Gitlab model can spoil that feature by splitting a change across multiple MRs that originate from personal forks. DrupalSpoons has taken great care the preserve this aspect of d.o. collaboration.

Each d.o. project (e.g. Devel) has two Gitlab.com projects - [one in the DrupalSpoons group](https://gitlab.com/drupalspoons/devel) and [a fork in the DrupalForks group](https://gitlab.com/drupalforks/devel). The key difference between the projects is that only the _Devel maintainers_ have _Developer role_ in the DrupalSpoons project. Only they can merge code into it. Everyone who wants to develop can have _Developer role_ in _DrupalForks_ after [applying for an account](docs/onboarding_user.md). All Git work happens in the DrupalForks project. There, devs collaborate freely and commit into each other's branches. When ready, anyone can open a Merge Request from a DrupalForks branch into a DrupalSpoons branch. From there, only a project maintainer can merge the code into DrupalSpoons.

- If you see an open merge request anywhere on DrupalSpoons, you can always make it better by pushing to its originating branch (which will be in DrupalForks). You have the permission and blessing to do so.
- All branches in forks are mirrored from their DrupalSpoons upstream. Refreshes are immediate. If a branch in a fork can't cleanly accept changes from its upstream, the branch is left untouched and anyone can manually fix it.
- DrupalForks is a simplified version of [issue workspaces](https://www.drupal.org/project/drupalorg/issues/2488266), so that no custom code is needed. Further, DrupalSpoons embraces Gitlab issues and MRs.
- Forks don't have issue queues. Issues always live in the DrupalSpoons project.
- Releases still happen on drupal.org. [Pushes from DrupalSpoons to drupal.org are not yet automated](https://gitlab.com/drupalspoons/webmasters/-/issues/8).
- Non-maintainers get the _Reporter_ role in all DrupalSpoons projects. That lets anyone change issue metadata on any issue, similar to Drupal.org model.
- There is still minor splitting of activity between an issue and its associated merge request. [Note that issues automatically list any mentioned merge request in its sidebar](https://docs.gitlab.com/ee/user/project/issues/issue_data_and_actions.html#related-merge-requests). This provides a standard navigation element for getting from an issue to its MR, and back. This splitting is also present with drupal.org __Issue Workspaces__ plan.

#### Local Development
- Browse [Devel's README](https://gitlab.com/drupalspoons/devel/#local-development) to setup local development.
- A simple `composer install` fetches Drupal core and any dependant modules. Further, the  module is symlinked into the /web/modules/project directory. This is a well encapsulated, composer-built site to develop with.
- This layout has great IDE integration since Drupal core is inside the project, offering completion and validation in the IDE. The IDE's composer integration works, PHPUnit integration works, etc.
- Docker is optional. It can be useful when you want to test on alternate Db platform or alternate PHP version, but its not required in everyday use.

#### CI/CD
- [A centrally managed a gitlab-ci.yml file](https://gitlab.com/drupalspoons/composer-plugin/-/blob/master/templates/.gitlab-ci.yml) that runs phpunit, code style, and deprecation tests. Unlike DrupalCI, projects can override any part of the template. For example, projects can add new services like Solr/Elasticsearch, or Redis/Memcache. Projects can use a different DB backend (e.g. Oracle) or custom PHP extensions.
- Projects can add custom checks, and save artifacts, etc.
- Like DrupalCI, the [build matrix](docs/ci.md) can vary by DB platform, PHP version, and Drupal core version.
- Scheduled Pipelines can run at any interval. This is useful for weekly testing of non-standard permutations. Users may run ad hoc pipelines against any permutation.
- [Getting started docs](docs/ci.md) for CI (and local development).
- All this is done without any custom code (we have no priveleges on gitlab.com).
- We stand upon the shoulders of giants: [DrupalCI](https://www.drupal.org/project/drupalci), [Drupal TI](https://github.com/LionsAd/drupal_ti/), [drupal-tests](https://github.com/deviantintegral/drupal_tests), [drupal_circleci](https://github.com/integratedexperts/drupal_circleci/blob/8.x/.circleci/build.sh), [Drush](https://github.com/drush-ops/drush/blob/master/tests/README.md).

#### Issues
- Browse [Devel Issues](https://gitlab.com/drupalspoons/devel/-/issues), to see issue list, issue search, and issue detail.
- [Full featured markdown](https://docs.gitlab.com/ee/user/markdown.html), including previews, drag and drop of images/video.... Also [quick actions](https://docs.gitlab.com/ee/user/project/quick_actions.html).
- [Scoped labels](https://gitlab.com/help/user/project/labels.md#scoped-labels-premium) like ~"state::fixed" and ~"priority::major" make it easy to filter and prioritize.
- [Description templates](https://docs.gitlab.com/ee/user/project/description_templates.html). [Devel re-uses drupal.org's template](https://gitlab.com/drupalspoons/devel/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).
- [Bulk editing](https://docs.gitlab.com/ee/user/project/bulk_editing.html#bulk-editing-issues-and-merge-requests) of issues and merge requests [without help from infrastructure team](https://www.drupal.org/project/infrastructure/issues/3132269).
- In-browser notifications, [a personal todo list](https://docs.gitlab.com/ee/user/todos.html), and [granular email notifications](https://docs.gitlab.com/ee/user/profile/notifications.html), reply via email.
- Dedicated [UX design workflow](https://gitlab.com/help/user/project/issues/design_management#design-management-premium).
- [Boards](https://docs.gitlab.com/ee/user/project/issue_board.html) for every project. [ContribKanbaan](https://contribkanban.com/) can transition to a satisfied retirement. Also useful for code sprints.
- Use mentions to notify anyone (e.g. @weitzman).
- Unresolved conversations in can be automatically copied into a follow-up issue.
- Voting (thumbs up/down) and Reactions.

#### Merge Requests
- Merge Requests (aka Pull Requests) replace the old patch workflow! [Enjoy fewer re-rolls](https://weitzman.github.io/blog/code-syntax), and [a web UI for resolving conflicts](https://about.gitlab.com/blog/2016/09/06/resolving-merge-conflicts-from-the-gitlab-ui/) that do happen.
- DrupalSpoons has line by line comments (no browser extension needed), and reviewers can [suggest a precise change](https://docs.gitlab.com/ee/user/discussions/#suggest-changes). This makes reviews more actionable, and less frustrating. Let's end nitpick culture, while keeping quality high.
- Projects can add a [CODEOWNERS](https://docs.gitlab.com/ee/user/project/code_owners.html) and optionally require Approval from the relevant owner in an MR. This file is a machine-readable version of [MAINTAINERS.txt](https://git.drupalcode.org/project/drupal/-/blob/8.9.x/core/MAINTAINERS.txt). Owners are shown as you [browse files in the Web UI](https://gitlab.com/drupalspoons/devel/-/blob/8.x-3.x/webprofiler/webprofiler.info.yml).
- Each MR has buttons for opening the branch in their IDE or editing further via a Web IDE.
- Strong web-based editing invites documentation authors and others into the Git workflow.

#### Other features
- [An incredibly full featured and well documented web services API](https://docs.gitlab.com/ee/api/). drupal.org's API is great but is read-only. This enables chat bots, alternate UIs for editing, migrations in or out, etc.
- For closed Issues and MRs, the _Assigned_ field records which participants are assigned contributor credit. We auto-publish [a Credits report](https://docs.google.com/spreadsheets/d/1m14yI71qwWpOS1WNXmF5GG6TbcuQlIlCUKT9tIob5F4/edit?usp=sharing) daily. Discuss at #2.
- [Keyboard shortcuts](https://docs.gitlab.com/ee/user/shortcuts.html) (or just press the <kbd>?</kbd> key)

#### Migration
- Example: [Devel Issues](https://gitlab.com/drupalspoons/devel/-/issues).
- [Issue migration project](https://gitlab.com/drupalspoons/drupal-issue-migration) fetches issue JSON from drupal.org and saves into a DrupalSpoons project.
- Care is taken to bring over all replies, issue metadata, etc. Closed issues are not migrated.
- The migration is [run via a Gitlab Pipeline](https://gitlab.com/drupalspoons/drupal-issue-migration/-/blob/master/README.md#run-via-curl) on an as needed basis. An admin specifies the source and target project ids.

Wishes: [Migrated issues/replies are from real users](https://gitlab.com/drupalspoons/drupal-issue-migration/-/issues/2), [open issues](https://gitlab.com/drupalspoons/drupal-issue-migration/-/issues).

#### Administration
- [Our channel on Drupal Slack](https://drupal.slack.com/archives/C013MP4UKC0).
- See [Onboarding a user to DrupalSpoons](docs/onboarding_user.md) and [Onboarding a project to DrupalSpoons](docs/onboarding_project.md)
- Project maintainers self-manage in a subgroup of [DrupalAdmins](https://gitlab.com/drupaladmins) that's named the same as their d.o. project (e.g. [Devel](https://gitlab.com/groups/drupaladmins/devel/-/group_members)). This subgroup is [shared](https://docs.gitlab.com/ee/user/project/members/share_project_with_groups.html#sharing-a-project-with-a-group-of-users) with the DrupalSpoons project (e.g. [Devel](https://gitlab.com/drupalspoons/devel/))
- Most Webmasters work happens in [Issues](https://gitlab.com/drupalspoons/webmasters/-/issues) and [Merge Requests](https://gitlab.com/drupalspoons/webmasters/-/merge_requests).

#### DrupalSpoons relationship to drupalcode.org
The Drupal Association (DA) is working on deeper integration between the Drupal.org issue queue and the Drupal Gitlab that lives at https://git.drupalcode.org. [Follow the progress](https://www.drupal.org/project/infrastructure/issues/3053290). Unfortunately the progress of this _Phase 2_ has been slow and the recent DrupalCon cancellation will not make things easier.

DrupalSpoons is here to provide a way for Drupal Contrib Modules to enjoy something similar to what is planned with the _Phase 2_. Plus it also is a demonstration that good workflows can be achieved without big changes to Gitlab.

In no way is this project intended to replace the service that the DA provides for drupal.org and nor will it make drupalcode.org obsolete.

#### Misc
- Star the projects you frequently visit so that they show in _Projects_ in upper left of all pages.
- Mark an issue as duplicate via the [quick action](https://gitlab.com/help/user/project/quick_actions). That has the benefit of adding a link to the top of the page. See #18.
