#!/usr/bin/env php
<?php

define('LEVEL_OWNER', 50);
define('LEVEL_MAINTAINER', 40);
define('LEVEL_DEVELOPER', 30);
define('LEVEL_REPORTER', 20);

use GuzzleHttp\Client;

require 'vendor/autoload.php';

$user_name = $argv[1]; // No @ at the beginning.
$slug = $argv[2];
if (!$token = getenv('GITLAB_TOKEN')) {
  throw new Exception('Missing Gitlab token.');
}

$config = [
  'headers' => ['Authorization' => 'Bearer '. $token],
  'http_errors' => true,
//  'debug' => true,
  'base_uri' => 'https://gitlab.com/api/v4/',
];
$client = new Client($config);
$drupalorg = new \GuzzleHttp\Client(['base_uri' => 'https://www.drupal.org']);

// Get a project. Used for exploring the API repsonse.
//$response = $client->get('projects/18900123', []);
//$body = $response->getBody();
//$json = json_decode($body);

# Set Group IDs.
$drupalspoons = 7657176;
$drupalforks = 7661092;
$drupaladmins = 7835720;

/**
 * @todo
 * - default issue/mr template
 * - prioritize labels.
 */

// Get project info from drupal.org, especially the title.
logg('Fetch project info from drupal.org');
  $response = $drupalorg->get("api-d7/node.json?&field_project_machine_name=" . $slug);
$json = json_decode($response->getBody());
$extension = $json->list[0];
if (!$title = $extension->title) {
  throw new Exception('Could not get Title from drupal.org');
}

// Get user_id based on name.
logg('Fetch user ID from Gitlab.');
$response = $client->get('users', ['query' => "username=$user_name"]);
$body = $response->getBody();
if (!$user_id = json_decode($body)[0]->id) {
  throw new Exception('Cannot find user. Don\'t include the @ at the beginning');
}

// Create subgroup for project maintainers.
logg('Create subgroup for maintainers.');
$params = [
  'name' => np_clean($title),
  'path' => $slug,
  'description' => "A group where *$slug* maintainers administer their own roles.",
  'visibility' => 'public',
  'project_creation_level' => 'noone',
  'subgroup_creation_level' => 'owner',
  'request_access_enabled' => FALSE,
  'parent_id' => $drupaladmins,
];
$response = $client->post('groups', ['form_params' => $params]);
$body = $response->getBody();
$json = json_decode($body);
$subgroup_id = $json->id;

// $subgroup_id = 8010261;

// Omit weitzman because he gets an error due to being a DrupalAdmins owner.
if ($user_name !== 'weitzman') {
  logg('Add requester to the subgroup.');
  $params = [
    'user_id' => $user_id,
    'access_level' => LEVEL_OWNER,
  ];
  $response = $client->post("groups/$subgroup_id/members", ['form_params' => $params]);
}

logg('Create project in DrupalSpoons, with code import from drupal.org');
// https://docs.gitlab.com/ee/api/projects.html#create-project
$params_project = [
  'name' => np_clean($title),
  'path' => $slug,
  'namespace_id' => $drupalspoons,
  'description' => "Clone and push code at [drupalforks/$slug](https://gitlab.com/drupalforks/$slug). [Administer maintainers](https://gitlab.com/groups/drupaladmins/$slug/-/group_members).",
  'issues_access_level' => 'enabled',
  'repository_access_level' => 'enabled',
  'merge_requests_access_level' => 'enabled',
  'forking_access_level' => 'enabled',
  'builds_access_level' => 'enabled',
  'wiki_access_level' => 'disabled', // @todo wont disable. handle on edit
  'snippets_access_level' => 'disabled', // @todo wont disable. handle on edit.
  'pages_access_level' => 'enabled',
  'container_registry_enabled' => false,
  'visibility' => 'public',
  'import_url' => "https://git.drupalcode.org/project/$slug.git",
  'public_builds' => true,
  'merge_method' => 'ff',
  'autoclose_referenced_issues' => true,
  'remove_source_branch_after_merge' => true,
  'request_access_enabled' => false,
  'auto_cancel_pending_pipelines' => 'enabled',
  'approvals_before_merge' => 0,
  'packages_enabled' => false,
  'service_desk_enabled' => false
];
$response = $client->post('projects', ['form_params' => $params_project]);
$body = $response->getBody();
$json = json_decode($body);
$project_id = $json->id;

// $project_id = 18971014;

logg('Wait for import to finish so we can get the default branch.');
while (1) {
  $response = $client->get("projects/$project_id", []);
  $body = $response->getBody();
  $json = json_decode($body);
  if ($default_branch = $json->default_branch) {
    break;
  }
  sleep(15);
}

// $project_id = 18087244; // spoons/Webmasters

logg('Delete existing protected branch.');
// https://docs.gitlab.com/ee/api/protected_branches.html#protect-repository-branches
$response = $client->delete("projects/$project_id/protected_branches/$default_branch", []);

// Create protected branch rule for new and old branch naming.
//$branch_params = [
//  'name' => '*.*.x',
//  'push_access_level' => LEVEL_DEVELOPER,
//  'merge_access_level' => LEVEL_DEVELOPER,
//  'unprotect_access_level' => LEVEL_MAINTAINER,
//];
//$response = $client->post("projects/$project_id/protected_branches", ['form_params' => $branch_params]);

logg('Share project with subgroup.');
// https://docs.gitlab.com/ee/api/projects.html#share-project-with-group
$params = [
  'group_id' => $subgroup_id,
  'group_access' => LEVEL_DEVELOPER,
];
$response = $client->post("projects/$project_id/share", ['form_params' => $params]);

// Prioritize labels
// https://docs.gitlab.com/ee/api/labels.html#edit-an-existing-label
// @todo getting 404
//$params = ['priority' => 100];
//$client->post("projects/$project_id/labels/14570489", ['form_params' => $params]);
//$params = ['priority' => 80];
//$client->post("projects/$project_id/labels/14570490", ['form_params' => $params]);
//$params = ['priority' => 60];
//$client->post("projects/$project_id/labels/14570500", ['form_params' => $params]);
//$params = ['priority' => 40];
//$client->post("projects/$project_id/labels/14570521", ['form_params' => $params]);

logg('Create project in DrupalForks, with code import from drupal.org');
$changes = [
  'namespace_id' => $drupalforks,
  'description' => null,
  'default_branch' => $default_branch,
  'issues_access_level' => 'disabled',
  'pages_access_level' => 'disabled',
];
$response = $client->post('projects', ['form_params' => array_merge($params_project, $changes)]);
$body = $response->getBody();
$json = json_decode($body);
$fork_id = $json->id;

// $fork_id = 18971017;

logg('Wait for import to finish so we can get the default branch.');
while (1) {
  $response = $client->get("projects/$fork_id", []);
  $body = $response->getBody();
  $json = json_decode($body);
  if ($default_branch = $json->default_branch) {
    break;
  }
  sleep(15);
}

logg('Delete existing protected branch.');
// https://docs.gitlab.com/ee/api/protected_branches.html#protect-repository-branches
$response = $client->delete("projects/$fork_id/protected_branches/$default_branch", []);

// Create protected branch rule for new and old branch naming. Nobody can use protected branches in forks.
//$branch_params = [
//  'name' => '*.*.x',
//  'push_access_level' => LEVEL_MAINTAINER,
//  'merge_access_level' => LEVEL_MAINTAINER,
//  'unprotect_access_level' => LEVEL_MAINTAINER,
//];
//$response = $client->post("projects/$project_id/protected_branches", ['form_params' => $branch_params]);

logg('Add fork relationship.');
# https://docs.gitlab.com/ee/api/projects.html#create-a-forked-fromto-relation-between-existing-projects
$response = $client->post("projects/$fork_id/fork/$project_id");

logg('Disallow more forks in upstream, and more');
// https://docs.gitlab.com/ee/api/projects.html#edit-project
$params = [
  'forking_access_level' => 'disabled',
  'wiki_access_level' => 'disabled',
  'snippets_access_level' => 'disabled',
  'packages_enabled' => false,
];
$response = $client->put("projects/$project_id", ['form_params' => $params]);

logg('Disable features in Fork that can only be done in an edit.');
// https://docs.gitlab.com/ee/api/projects.html#edit-project
$params = [
  'forking_access_level' => 'disabled',
  'wiki_access_level' => 'disabled',
  'snippets_access_level' => 'disabled',
  'packages_enabled' => false,
];
$response = $client->put("projects/$fork_id", ['form_params' => $params]);

function np_clean($string) {
  return str_replace(':', '-', $string);
}

function logg($message) {
  fwrite(STDERR, "$message\n");
}




