<?php

require '../vendor/autoload.php';

$password = getenv('WEBHOOK_PWD_MIRROR');
// Its an env variable on Heroku, and otherwise in _SERVER.
$http_password = getenv('HTTP_X_GITLAB_TOKEN') ?: $_SERVER['HTTP_X_GITLAB_TOKEN'];
if (empty($http_password)) {
  logg('Missing hook password.');
  die();
}
if ($http_password !== $password) {
  logg('Invalid hook password..');
  die();
}

// GitLab sends the json as raw post data
$input = file_get_contents("php://input");
$json  = json_decode($input);
$is_new = $json->object_attributes->action == 'open';
$title = trim($json->object_attributes->title);
$author_id = $json->object_attributes->author_id;
$issue_iid = $json->object_attributes->iid;
$project_id = $json->project->id;
$webmasters = 18087244;

if (!$is_new) {
  logg('Its an update. Nothing to do.');
  exit(0);
}
if ($title !== 'Join DrupalSpoons') {
  logg('Not a Join DrupalSpoons issue. Nothing to do.');
  exit(0);
}

// Build a client for Gitlab API
if (!$trigger = getenv('GITLAB_TRIGGER_NEW_ISSUE')) {
  throw new Exception('Missing trigger. Cannot trigger new Pipeline.');
}
$config = [
  'http_errors' => true,
  'base_uri' => 'https://gitlab.com/api/v4/',
];
$client = new \GuzzleHttp\Client($config);

logg("Trigger the new_user pipeline for $issue_iid in $project_id.");
$response = $client->post("projects/$webmasters/trigger/pipeline?token=$trigger&ref=master&variables[PROJECT_ID]=$project_id&variables[ISSUE_ID]=$issue_iid");
$body = $response->getBody();
$json = json_decode($body);

function logg($message) {
  file_put_contents('php://stderr', "$message\n");
}
